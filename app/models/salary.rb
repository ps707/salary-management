class Salary < ApplicationRecord
  belongs_to :user
  validates :salary_type, presence: true
  validates :amount, :user_id, presence: true
  validates_with SalaryValidator

  enum salary_type: { basic: 0, hardware_allowance: 1, daily_allowance: 2, TDA: 3 }
end

