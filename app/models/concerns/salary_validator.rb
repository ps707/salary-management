class SalaryValidator < ActiveModel::Validator
  def validate(record)
    # binding.pry
    if check_salary(record)
      record.errors[:base] << "this salary type already exist for this user"
    end
  end

  def check_salary(record)
    !Salary.where(user_id: record.user_id, salary_type: record.salary_type).empty?
  end
end