class User < ApplicationRecord
  belongs_to :role
  has_one :profile, dependent: :destroy
  has_many :salaries, dependent:  :destroy

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JWTBlacklist

  validates :role_id, presence: true

  accepts_nested_attributes_for :profile
end
