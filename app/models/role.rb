class Role < ApplicationRecord
  has_many :users
  validates :name, presence: true

  enum name: { admin: 0, cto: 1, developers: 2, manager: 3 }
end
