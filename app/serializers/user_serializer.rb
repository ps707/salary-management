class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :role_id

  has_one :role, key: :role_attributes
  has_one :profile, key: :profile_attributes
  has_many :salaries, key: :salary_attributes
end