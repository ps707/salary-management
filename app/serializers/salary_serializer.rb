class SalarySerializer < ActiveModel::Serializer
  attributes(
    :id,
    :salary_type,
    :amount
  )
end