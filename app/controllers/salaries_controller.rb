class SalariesController < ApplicationController
  before_action :authenticate_user!

  def index
    # binding.pry
    render json: Salary.where(user: params[:user_id])
  end

  def create
    @salary = Salary.new(salary_params)
    # binding.pry
    if @salary.save
      render json: salary
    else
      render json: {
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: @salary.errors,
            code: '100'
          }
        ]
      }, status: :bad_request
    end
  end

  def update
    if salary.update(salary_params)
      render json: salary
    else
      render json: {
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: salary.errors,
            code: '100'
          }
        ]
      }, status: :bad_request
    end
  end

  def destroy
    # binding.pry
    salary.destroy
  end

  private

  def salary
    @salary ||= Salary.find(params[:id])
  end

  def salary_params
    params.require(:salary).permit(
      :salary_type,
      :amount,
      :user_id
    )
  end
end
