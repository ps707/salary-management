class RegistrationsController < Devise::RegistrationsController
  # skip_before_filter :verify_authenticity_token, :only => :create

  def create
    build_resource(sign_up_params)
    resource.save
    render_resource(resource)
  end

  private

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role_id,
                                 profile_attributes: [:first_name, :last_name, :salution, :designation])
  end
end