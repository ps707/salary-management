class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    render json: User.all
  end

  def show
    render json: user
  end

  def update
    if user.update(update_params)
      render json: user
    else
      render json: {
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: user.errors,
            code: '100'
          }
        ]
      }, status: :bad_request
    end
  end

  def destroy
    # binding.pry
    user.destroy
  end

  private

  def user
    @user ||= User.find(params[:id])
  end

  def update_params
    params.require(:user).permit(
      :email,
      :role_id,
      profile_attributes: [
        :id,
        :first_name,
        :last_name,
        :salution,
        :designation
      ]
    )
  end
end
