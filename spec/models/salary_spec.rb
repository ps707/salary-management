require 'rails_helper'

RSpec.describe Salary, type: :model do
  describe 'Validations' do
    it {should validate_presence_of(:user_id)}
    it {should validate_presence_of(:amount)}
    it {should validate_presence_of(:salary_type)}
    # it {should validate_inclusion_of(:salary_type).in_range(0..3)}
  end

  describe 'Associations' do
    it { should belong_to(:user)}
  end
end
