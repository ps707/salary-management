require 'rails_helper'

RSpec.describe Profile, type: :model do
  describe 'Validations' do
    it {should validate_presence_of(:first_name)}
    it {should validate_presence_of(:last_name)}
    it {should validate_presence_of(:salution)}
    it {should validate_presence_of(:designation)}
  end

  describe 'Associations' do
    it { should belong_to(:user)}
  end
end
