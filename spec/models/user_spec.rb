require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Validations' do
    it {should validate_presence_of(:role_id)}
    # it {should validate_inclusion_of(:role_id).in_range(0..3)}
  end

  describe 'Associations' do
    it { should belong_to(:role)}
    it { should have_one(:profile)}
  end
end
