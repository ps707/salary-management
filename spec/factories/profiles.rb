FactoryBot.define do
  factory :profile do
    first_name "Prakash"
    last_name "Sanyasi"
    salution "Mr."
    designation "Backend Developer"
  end
end
