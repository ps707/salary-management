require 'rails_helper'

describe 'User login' do
  let!(:user_1) {
    create(:user, role: Role.first,
            profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
    )
  }

  context 'With valid email to log in' do
    it 'Renders valid user token' do
      post user_session_path, params: { user: { email: user_1.email, password: user_1.password } }
      expect(status).to eq(200)
      expect(response.headers['Authorization']).to be_present
      # expect(response.headers['Authorization']).not_to eq(nil)
    end
  end

  context 'With invalid email to log in' do
    it 'throws invalid login error' do
      post user_session_path, params: { user: { email: 'test@mail.com', password: 'password' } }
      expect(status).to eq(401)
      expect(json.dig("error")).to eq('Invalid Email or password.')
    end
  end
end