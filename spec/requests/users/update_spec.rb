require 'rails_helper'

describe 'Users' do
  let!(:user) {
    create(:user, role: Role.first,
            profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
    )
  }
  let!(:token) {user_token(user)}
  let!(:params) {
    {
      user: {
        profile_attributes: {
          id: user.profile.id,
          first_name: 'Namgay'
        }
      }
    }
  }

  it 'user details get updated' do
    put user_path(user), params: params, headers: header_params(token: token)
    # binding.pry
    expect(status).to eq(200)
    expect(json.dig("user", "profile_attributes","first_name")).to eq('Namgay')
  end

  let!(:invalid_params) {
    {
      user: {
        profile_attributes: {
          id: user.profile.id,
          first_name: ''
        }
      }
    }
  }
  it 'user details does not get updated with invalid params' do
    put user_path(user), params: invalid_params, headers: header_params(token: token)
    # binding.pry
    expect(status).to eq(400)
    expect(json.dig("errors",0,"detail","profile.first_name",0)).to eq("can't be blank")
  end
end