require 'rails_helper'

describe 'Users' do
  let!(:user) {
    create(:user, role: Role.first,
            profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
    )
  }
  let!(:token) {user_token(user)}

  it 'Gets #show' do
    get user_path(user), params: {}, headers: header_params(token: token)
    expect(status).to eq(200)
    # binding.pry
    expect(json.dig("user", "email")).to eq(user.email)
    expect(json.dig("user", "profile_attributes")).to be_present
    expect(json.dig("user", "role_attributes")).to be_present
  end
end