require 'rails_helper'

describe 'User logout' do
  let!(:url) {'/logout'}

  it 'returns 204, no content' do
    delete url
    expect(response).to have_http_status(204)
  end
end