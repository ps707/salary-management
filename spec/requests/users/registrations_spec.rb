require 'rails_helper'

describe 'User registration' do
  context 'With valid fields' do
    let!(:params) {
      {
        user: {
          email: "prakash.sanyasi@selise.ch",
          password: 'password',
          role_id: Role.first.id,
          profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
        }
      }
    }

    it 'Allows user to request for sign up' do
      post user_registration_path, params: params
      # binding.pry
      expect(status).to eq(200)
      expect(json.dig("user", "email")).to eq('prakash.sanyasi@selise.ch')

      user = User.find(json.dig("user", "id"))
      expect(user.role.name).to eq('admin')
    end
  end

  context 'With invalid email field' do
    let!(:params) {
      {
        user: {
          email: 'testmail',
          password: 'password',
          role_id: Role.first.id,
          profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
        }
      }
    }
    it 'Does not allow users to request for sign up' do
      post user_registration_path, params: params
      expect(status).to eq(400)
      # binding.pry
      expect(json.dig("errors", 0, "detail", "email", 0)).to eq('is invalid')
    end
  end

  context 'With invalid password field' do
    let!(:params) {
      {
        user: {
          email: 'testmail@gmail.com',
          password: 'word',
          role_id: Role.first.id,
          profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
        }
      }
    }
    it 'Does not allow users to request for sign up' do
      post user_registration_path, params: params
      expect(status).to eq(400)
      # binding.pry
      expect(json.dig("errors", 0, "detail", "password", 0)).to eq('is too short (minimum is 6 characters)')
    end
  end

   context 'With invalid first and last name field' do
    let!(:params) {
      {
        user: {
          email: 'testmail@gmail.com',
          password: 'password',
          role_id: Role.first.id,
          profile_attributes: {
            first_name: "",
            last_name: "",
            salution: "Mr.",
            designation: "Backend Developer"
          }
        }
      }
    }
    it 'Does not allow users to request for sign up' do
      post user_registration_path, params: params
      expect(status).to eq(400)
      # binding.pry
      expect(json.dig("errors", 0, "detail", "profile.first_name", 0)).to eq('can\'t be blank')
      expect(json.dig("errors", 0, "detail", "profile.last_name", 0)).to eq('can\'t be blank')
    end
  end

  context 'when user already exists' do
    let!(:params) {
      {
        user: {
          email: 'testmail@gmail.com',
          password: 'password',
          role_id: Role.first.id,
          profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
        }
      }
    }
    let!(:user_1) {
      create(:user, email: 'testmail@gmail.com', role: Role.first,
          profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
      )
    }

    it 'Does not allow users to request for sign up' do
      post user_registration_path, params: params
      expect(status).to eq(400)
      # binding.pry
      expect(json.dig("errors", 0, "detail", "email", 0)).to eq('has already been taken')
    end
  end
end
