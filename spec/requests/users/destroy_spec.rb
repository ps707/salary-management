require 'rails_helper'

describe 'User' do
  let!(:user) {
    create(:user, role: Role.first,
           profile_attributes: {
             first_name: "Prakash",
             last_name: "Sanyasi",
             salution: "Mr.",
             designation: "Backend Developer"
           }
    )
  }
  let!(:user_1) {
    create(:user,
           email: "psanyasi@acm.org",
           role: Role.first,
           profile_attributes: {
             first_name: "Prakash",
             last_name: "Sanyasi",
             salution: "Mr.",
             designation: "Backend Developer"
           }
    )
  }
  let!(:token) {user_token(user)}

  describe 'GET #destroy' do
    it 'should delete the user' do
      # binding.pry
      expect {delete user_path(user_1), params: {}, headers: header_params(token: token)}.to change(User, :count).by(-1)
    end
  end
end