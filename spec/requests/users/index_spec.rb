require 'rails_helper'

describe 'User' do
  let!(:user) {
    create(:user, role: Role.first,
            profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
    )
  }
  let!(:token) {user_token(user)}

  describe 'GET #index' do
    it 'renders all users' do
      get users_path, params: {}, headers: header_params(token: token)
      # binding.pry
      expect(status).to eq(200)
    end
  end
end