require 'rails_helper'

describe 'Salary' do
  let!(:user) {
    create(:user, role: Role.first,
            profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
    )
  }
  let!(:token) {user_token(user)}
  let!(:salary){create(:salary, user: user)}

  describe 'GET #index' do
    it 'render particular salary' do
      # binding.pry
      get salaries_path, params: {user_id: user.id}, headers: header_params(token: token)
      # binding.pry
      expect(status).to eq(200)
      expect(json.dig("salaries",0,"salary_type")).to eq("hardware_allowance")
    end
  end
end