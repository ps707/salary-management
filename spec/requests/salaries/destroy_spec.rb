require 'rails_helper'

describe 'Salary' do
  let!(:user) {
    create(:user, role: Role.first,
            profile_attributes: {
            first_name: "Prakash",
            last_name: "Sanyasi",
            salution: "Mr.",
            designation: "Backend Developer"
          }
    )
  }
  let!(:token) {user_token(user)}
  let!(:salary){create(:salary, user: user)}

  describe 'Delete #destory' do
    it 'render particular salary' do
      # binding.pry
      expect {delete salary_path(salary), params: {}, headers: header_params(token: token)}.to change(Salary, :count).by(-1)
    end
  end
end