require 'rails_helper'

describe 'Salary' do
  let!(:user) {
    create(:user, role: Role.first,
           profile_attributes: {
             first_name: "Prakash",
             last_name: "Sanyasi",
             salution: "Mr.",
             designation: "Backend Developer"
           }
    )
  }
  let!(:token) {user_token(user)}
  let!(:params) {
    {
      salary: {
        salary_type: 'basic',
        amount: 50000.00,
        user_id: user.id
      }
    }
  }
  let!(:invalid_params) {
    {
      salary: {
        salary_type: '',
        amount: 50000.00,
        user_id: user.id
      }
    }
  }

  describe 'POST #create' do
    it 'create a salary with valid data' do
      # binding.pry
      post salaries_path, params: params, headers: header_params(token: token)
      expect(status).to eq(200)
      expect(json.dig("salary","amount")).to eq("50000.0")
    end

    it 'does not create a salary with invalid data' do
      post salaries_path, params: invalid_params, headers: header_params(token: token)
      expect(status).to eq(400)
      # binding.pry
      expect(json.dig("errors",0,"detail","salary_type",0)).to eq("can't be blank")
    end
  end

end