require 'rails_helper'

describe 'Salary' do
  let!(:user) {
    create(:user, role: Role.first,
           profile_attributes: {
             first_name: "Prakash",
             last_name: "Sanyasi",
             salution: "Mr.",
             designation: "Backend Developer"
           }
    )
  }
  let!(:token) {user_token(user)}
  let!(:salary){create(:salary, user: user)}
  let!(:params) {
    {
      salary: {
        salary_type: 'TDA',
      }
    }
  }
  let!(:invalid_params) {
    {
      salary: {
        salary_type: ''
      }
    }
  }

  describe 'PUT #update' do
    it 'update salary with valid data' do
      # binding.pry
      put salary_path(salary), params: params, headers: header_params(token: token)
      expect(status).to eq(200)
      # binding.pry
      expect(json.dig("salary","salary_type")).to eq("TDA")
    end

    it 'does not update salary with invalid data' do
      put salary_path(salary), params: invalid_params, headers: header_params(token: token)
      expect(status).to eq(400)
      # binding.pry
      expect(json.dig("errors",0,"detail","salary_type",0)).to eq("can't be blank")
    end
  end

end