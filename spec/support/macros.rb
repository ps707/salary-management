# def login_user
#   before(:each) do
#     @requests.env["devise.mapping"] = Devise.mappings[:user]
#     # binding.pry
#     user = FactoryBot.create(:user)
#     sign_in user
#   end
# end

def json
  JSON.parse(response.body)
end

def user_token(user)
  post user_session_path, params: {user: {email: user.email, password: user.password}}
  response.headers['Authorization']
end

def header_params(args = {})
  {'Authorization': args[:token], 'Accept': 'Application/json', 'HTTP_ACCEPT_LANGUAGE': args[:locale] || 'en'}
end