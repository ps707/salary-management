class CreateSalaries < ActiveRecord::Migration[5.2]
  def change
    create_table :salaries do |t|
      t.integer :type
      t.decimal :amount
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
