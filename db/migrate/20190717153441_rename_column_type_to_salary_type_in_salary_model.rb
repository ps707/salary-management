class RenameColumnTypeToSalaryTypeInSalaryModel < ActiveRecord::Migration[5.2]
  def change
    rename_column :salaries, :type, :salary_type
  end
end
