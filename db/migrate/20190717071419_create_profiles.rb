class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :salution
      t.string :designation
      t.references :user

      t.timestamps
    end
  end
end
